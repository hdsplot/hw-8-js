let paragraph = document.querySelectorAll('p');
paragraph.forEach(elem => { elem.style.backgroundColor = "#ff0000" });

let optionsList = document.getElementById('optionsList');
console.log(optionsList);

let optionsListParent = document.getElementById('optionsList').parentNode;
console.log(optionsListParent);

let optionsListChilds = document.getElementById("optionsList").childNodes;
console.log(optionsListChilds, typeof optionsListChilds);

document.getElementById('testParagraph').textContent = 'This is a paragraph';

let mainHeaderElements = document.querySelector('.main-header').children;
console.log(mainHeaderElements);
for (let i = 0; i < mainHeaderElements.length; i++) {
    mainHeaderElements[i].classList.toggle('nav-item');
}
console.log(mainHeaderElements);

let optionsListTitle = document.querySelectorAll('.options-list-title');
console.log(optionsListTitle);
optionsListTitle.forEach(elem => { elem.classList.remove('options-list-title') });
console.log(optionsListTitle);